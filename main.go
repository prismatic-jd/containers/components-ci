package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httputil"
	"os"
	"os/exec"
)

func triggerPipeline(projectID string, token, ref, component string) {
	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/trigger/pipeline", projectID)

	body := &bytes.Buffer{}
	form := multipart.NewWriter(body)
	form.WriteField("token", token)
	form.WriteField("ref", ref)
	form.WriteField("variables[COMPONENT]", component)
	form.Close()

	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		panic(err)
	}

	req.Header.Set("Content-Type", form.FormDataContentType())
	dump, err := httputil.DumpRequest(req, true)
	if err != nil {
		fmt.Println("Error dumping request:", err)
		return
	}

	fmt.Printf("%s", dump)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		fmt.Fprintf(os.Stderr, "Failed to trigger pipeline for %s: %s\n", component, string(bodyBytes))
	} else {
		fmt.Printf("Triggered pipeline for %s\n", component)
	}
}

func iterateSubdirectories(dirPath string) {
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		panic(err)
	}
	for _, file := range files {

		if file.IsDir() {

			projectID := os.Getenv("CI_PROJECT_ID")
			token := os.Getenv("CI_JOB_TOKEN")
			ref := os.Getenv("CI_COMMIT_REF_NAME")
			component := file.Name()

			// Check if there are any changes in the current subdirectory
			cmd := exec.Command("git", "diff", "--name-only", "HEAD", fmt.Sprintf("%s/", component))
			output, err := cmd.Output()
			if err != nil {
				panic(err)
			}
			if len(output) == 0 {
				fmt.Printf("No changes found in %s, skipping pipeline trigger\n", component)
				continue
			}
			triggerPipeline(projectID, token, ref, component)
		}
	}
}

func main() {
	args := os.Args
	iterateSubdirectories(args[1])
}
