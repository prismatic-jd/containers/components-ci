# Specify the base image for this Dockerfile
FROM registry.gitlab.com/prismatic-jd/containers/go:1.20 as builder

# Set the working directory inside the container
WORKDIR /home/service

# Copy the main.go file from the current directory to the container's working directory
COPY main.go .

# Build the executable binary of the Go program
RUN go build main.go

# Specify another base image for the final container image
FROM registry.gitlab.com/prismatic-jd/containers/base:0.1.0

# Create a user called "service" with UID and GID set to 1000
# RUN adduser -D -u 1000 -g 1000 service

# # Set the working directory inside the container to "/service"
WORKDIR /service

# # Change ownership of the "/service" directory and all its contents to the "service" user
# RUN chown -R service:service /service

# # Switch to the "service" user
# USER service

# Copy the binary file "main" from the previous stage (i.e. "builder") to the current directory
COPY --from=builder /home/service/main ./builder
